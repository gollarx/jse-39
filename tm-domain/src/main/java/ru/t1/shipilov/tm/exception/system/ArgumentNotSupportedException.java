package ru.t1.shipilov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@Nullable final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
